// alert('hi');

// A document is your webpage
console.log(document);
// Targeting the first name input field

// document.querySelector('#txt-first-name');

// alternatively:

// document.getElementById('txt-first-name');

// for other types
	// document.getElementByClassName('class-name');
	// document.getElementByTagName('tag-name');

// Contain the query selector code in a constant

// const txtFirstName = document.querySelector("#txt-first-name");

// const spanFullName = document.querySelector("#span-full-name");

// const txtLastName = document.querySelector("#txt-last-name");

// // Event Listeners

// txtFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = txtFirstName.value;
// });


// // Multiple Listeners
// 	// e is a shorthand for event
// txtFirstName.addEventListener('keyup', (e) => {
// 	console.log(e.target);
// 	console.log(e.target.value);
// });

// activity

const txtFirstName = document.querySelector("#txt-first-name");

const txtLastName = document.querySelector("#txt-last-name");

const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});

txtLastName.addEventListener('keyup', (event2) => {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
});

// spanFullName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = spanFullName.value;
// });

// document.body.addEventListener('keyup', (event) => {
// 	if (event.target !== txtFirstName && event.target !== txtLastName) {
// 		return
// 	}
// });

// document.querySelectorAll('spanFullName').forEach(item => {
// 	item.addEventListener('keyup', event => {

// 	})
// })
